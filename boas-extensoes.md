Essa as aqui são algumas das extensões que utilizo no Chrome que me ajudam a trabalhar

## Nimbus Screenshot & Screen Video Recorder

Essa extensão é focada em capturar páginas da web, janelas de navegação inteiras ou partes delas, edite as capturas de telas e salve as imagens em arquivos. Utilizo ela para tirar os prints das páginas em desenvolvimento e dar os feedbacks para melhorias.

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj)

* [Video Tutorial da extensão](https://www.youtube.com/watch?v=IyzNfeI2SrQ)


## Lighthouse

O Lighthouse é uma ferramenta automatizada e de código aberto para melhorar o desempenho, a qualidade e a exatidão de seus aplicativos da web. 

Ao auditar uma página, o Lighthouse executa uma série de testes contra a página e gera um relatório sobre o desempenho da página. A partir daqui, você pode usar os testes com falha como indicadores sobre o que você pode fazer para melhorar seu aplicativo.

[Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk)

* Guia de início rápido sobre o uso do Lighthouse:
http://bit.ly/lighthouse-quickstart

* Veja e compartilhe relatórios online:
https://googlechrome.github.io/lighthouse/viewer/

* Fonte e detalhes do GIthub:
https://github.com/GoogleChrome/lighthouse


## CSS Peeper

🔎 Não mais cavar em um código. Inspecione os estilos de maneira simples, organizada e bonita. Obtê-lo agora!

O CSS Peeper é um visualizador CSS feito sob medida para os designers. Tenha acesso aos estilos úteis com nossa extensão do Google Chrome. Nossa missão é permitir que os Designers se concentrem no design e gastem o menor tempo possível em um código.

Já imaginou qual seria a altura da linha, a fonte ou o tamanho de um botão em um site? Nós fornecemos-lhe a melhor ferramenta para satisfazer sua curiosidade. Nós permitimos que você inspecione o código da maneira mais fácil possível. Verifique o estilo CSS oculto de objetos, cores e recursos na web.

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/css-peeper/mbnbehikldjhnfehhnaidhjhoofhpehk)

* [Site do CSS Peeper](https://csspeeper.com/)


## AWRStudyr

O AWRStudyr é uma extensão do Chrome que oferece percepções de SEO e tecnologias da web sempre que você abre uma nova página da web. Obtenha insights avançados sobre tecnologias da web sempre que você abrir uma nova página da web.

- Teste compatível com dispositivos móveis
- pontuações do Google PageSpeed
- validação de HTML5
- Verificador de acessibilidade
- Detecção de conteúdo misto
- e mais...

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/awrstudyr/mbkehkfjhncahcaggkncdaacfnikmoid)
* [Site da extensão](http://www.awrstudyr.com/)


## Page Ruler

Desenhe uma régua para obter dimensões e posicionamento em pixels e meça elementos em qualquer página da web.
A régua de páginas permite que você desenhe uma régua em qualquer página e exiba a largura, altura e posição dela.

**Características**

* Desenhe uma régua em qualquer página e veja a largura, altura e posição superior, inferior, esquerda e direita
* Arraste as bordas da régua para redimensioná-la
* Use as setas do teclado para mover e redimensionar a régua
* Mostrar guias que se estendem das bordas da régua
* Atualize manualmente o tamanho e a posição da régua da barra de ferramentas para fazer alterações de precisão
* Ative o "Element Mode" para delinear elementos na página enquanto você move o mouse sobre eles
* Navegue pelos pais, filhos e irmãos de qualquer elemento medido


**Importante**

Para usar em páginas locais, você precisará ativar a opção "permitir acesso a URLs de arquivos" na guia "Extensões".

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/page-ruler/emliamioobfffbgcfdchabfibonehkme)

## Woorank

WooRank oferece uma profunda SEO e Analise site rever mais de 80 criterios de SEO.
Relatório de SEO é uma extensão feita pelo WooRank. Este plugin SEO fornece um relatório de SEO muito profundo para qualquer site.

WooRank fornece muitas dicas de SEO para o seu site para classificar o número 1 nos motores de busca. É uma ferramenta poderosa para profissionais de marketing na Internet, designers, especialistas em usabilidade de sites, desenvolvedores da Web e móveis e outros profissionais digitais.

Quando você clica no ícone da extensão WooRank, a extensão abre um pop-up com uma análise de SEO do site, abrangendo uma série de fatores.

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/seo-website-analysis/hlngmmdolgbdnnimbmblfhhndibdipaf)

## Katalon Recorder (Selenium IDE para Chrome)

Melhor registro de Selenium IDE, jogo, app de depuração. Exporta o código do Selenium WebDriver. Fornece relatórios, registros, capturas de tela. Rápido e extensível
O Katalon Automation Recorder é o novo Selenium IDE que ajuda a registrar ações, capturar elementos da Web em aplicativos da Web, executar casos de teste automatizados e fazer relatórios com rapidez e facilidade. O Katalon Recorder suporta os comandos legados e scripts de extensão do Selenium IDE (AKA user-extensions.js) para desenvolver construtores e ações de localizadores personalizados.

** Não se esqueça de verificar outras soluções de teste gratuitas do Katalon em https://katalon.com.

O Katalon Recorder tornará o seu trabalho de automatização de testes muito mais fácil.
* Grave, reproduza, depure com controle de velocidade, pause / resume, capacidades de breakpoints.
* Aproveite a velocidade de execução mais rápida em comparação com outras extensões com o motor central Selenium 3.
* Faça uso de vários tipos de localizadores, incluindo XPath e CSS.
* Use comandos originais do Selenium IDE (Selenese), além de instruções block se ... elseIf ... else ... endIf e while ... endWhile. O controle de entrada do arquivo de teste é suportado.
* Importe dados de teste de arquivos CSV para testes controlados por dados.
* Relatório facilmente com registros, screenshots capturando, com dados históricos e análises de Katalon Analytics.
* Compor e organizar casos de teste em suítes. Nunca perca seu trabalho com o recurso de gravação automática.
* Import original Selenium IDE (Firefox extension) tests.
* Exportar para scripts Selenium WebDriver nestes frameworks: C # (MSTest e NUnit), Java (TestNG e JUnit), Ruby (RSpec), Python (unittest), Groovy (Katalon Studio), Robot Framework e XML.
* Plataforma de plugins para ampliar os recursos do Katalon Recorder. Leia mais em https://forum.katalon.com/discussion/5501/introduce-plugin-platform-in-katalon-recorder-3-4-0.
* Suporta antigas user-extensions.js do Selenium IDE https://forum.katalon.com/discussion/6286/we-have-released-katalon-recorder-3-5-0-with-support-for-extension-scripts- aka-user-extensions-js.

O Katalon Recorder está disponível com os navegadores Firefox e Chrome mais recentes. O desenvolvimento é muito ativo com a nova versão atualizada automaticamente semanalmente.

Links Úteis:
* Notas de lançamento https://docs.katalon.com/x/JwHR
* Amostras https://docs.katalon.com/x/KQHR
* Katalon Recorder vs Katalon Studio comparação https://docs.katalon.com/x/cAHR

## Checkbot: SEO, velocidade da Web e testador de segurança 🚀

O Checkbot informa como aumentar o SEO, a velocidade da página e a segurança da Web do seu site, rastreando e testando milhares de páginas em minutos para encontrar problemas que você ignorou: desvendar links quebrados, problemas de conteúdo duplicados, HTML / CSS / JavaScript inválidos formulários de senha inseguros, cadeias de redirecionamento que matam o desempenho, erros mistos de conteúdo e muito mais. 🚀

O Checkbot testa 50+ melhores práticas com base em recomendações de especialistas da Web, como Google, Mozilla, W3C e OWASP. O Checkbot dá a você a liberdade de rastrear qualquer site que desejar com a frequência que desejar, incluindo sites de desenvolvimento local, para que você possa identificar e corrigir problemas críticos da Web antes que eles cheguem ao seu site ativo.

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/seo-web-speed-security-te/dagohlmlhagincbfilmkadjgmdnkjinl)
* [Site da extensão](https://www.checkbot.io/?utm_source=chrome-web-store)

## Emmet Review

O Emmet Re: View exibe sua página da Web responsiva em várias visualizações lado a lado, para que você possa testar rapidamente como a página da Web analisa diferentes resoluções e dispositivos. Todas as visualizações são totalmente sincronizadas: role, preencha os campos do formulário, clique, passe o mouse e arraste os elementos em uma visualização e obtenha feedback instantâneo em todos os outros.

* [Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/emmet-review/epejoicbhllgiimigokgjdoijnpaphdp)
* [Site da extensão](http://re-view.emmet.io/)

## PixelParallel

O PixelParallel é uma ferramenta de comparação de HTML vs Design completamente livre, super acessível e leve para desenvolvedores de front-end que o ajudará a codificar sites perfeitos de pixel com facilidade. 

Construído de desenvolvedores para desenvolvedores, o PixelParallel é uma ferramenta que trabalha em favor da precisão final na codificação de front-end.

Como funciona:
1. Basta carregar sua imagem e comparar a sobreposição semitransparente com a página da web abaixo. 
2. Personalize a opacidade, escala e posição da imagem como você deseja. Você também pode ocultá-lo ou bloqueá-lo.
3. Faça a diferença de cor para obter uma precisão perfeita de pixel forte. 
4. Use as grades verticais e horizontais para obter um design de pixel perfeito.

Características:

- Grade de bootstrap com largura personalizável, número de colunas, medianiz e opacidade
- Linhas horizontais com espaço personalizável entre
- Réguas arrastáveis ​​(como no Photoshop) + opção Redefinir
- Inversão (diferença de cor) 
- Sobreposições separadas para cada página da web
- Escala de sobreposição
- Opacidade de sobreposição personalizável
- Salvando sobreposições entre sessões
- Todos os tamanhos de imagem suportados
- Sobreposição arrastável
- Ocultar rapidamente / mostrar imagem
- Atalhos + atalhos de teclado personalizáveis
- Funciona com arquivos locais
- Interface intuitiva: limpa e simples; arrastável; minimizável


[Video tutorial da extensão](https://www.youtube.com/watch?v=tt02nS-cGMI)

[Github do projeto](https://github.com/htmlburger/pixelparallel)

[Instalar extensão no Chrome](https://chrome.google.com/webstore/detail/pixelparallel-by-htmlburg/iffnoibnepbcloaaagchjonfplimpkob)

