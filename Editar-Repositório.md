## **Realizar suporte de repositório já publicado**
> Se precisar modificar algo do site já publicado realize os seguintes passos com MUITO CUIDADO para não editar o master.
Se editar o master o que acontece? Nada, só dará trabalho para sua vida.
___________
### **1. Entrar no ramo homologacao**
```sh
$ git checkout homologacao
```

___________
### **2. Crie um novo branch com o ticket do suporte**
> É necessário que seja criado este ramo para que haja facilidade em realizar suportes posteriores. As alterações do suporte serão realizadas nesta nova branch

```sh
$ git checkout -b ticket_x
```

___________
### **3. Envie o branch**
> Um branch não está disponível a outros a menos que você envie o branch para seu repositório remoto.

```sh
$ git push origin ticket_x
```

___________
### **4. Abra seu codigo do branch ticket_x**
> Lembre-se sempre de confirmar que está no ramo correto.

```sh
$ git checkout ticket_x
```

___________
### **5. Realize o suporte**
> Modifique seu código do branch ticket_x. Para segurança realize commits periodicamente.
```sh
$ git add .
$ git commit -m "abcdefghi"
$ git push origin ticket_x
```
___________
### **6. Subir Suporte**
> Após realizar as modificações necessárias, é necessário realizar um merge delas tanto no ramo principal (master) quanto no homologacao.
Ao realizar este merge não se preocupe com as configurações do banco de dados e da url, não serão modificados.
Entretanto, se você modificou estes arquivos, será necessário realizar todos os passos novamente, ou seja, criar um novo homologacao.

```sh
$ git checkout homologacao
$ git merge ticket_x
$ git add .
$ git commit -m "Realizado suporte ticket_x"
$ git push origin homologacao
```
```sh
$ git checkout master
$ git merge ticket_x
$ git add .
$ git commit -m "Realizado suporte ticket_x"
$ git push origin master
```
___________
### **7. Excluir branch ticket_x**
```sh
$ git branch -d ticket_x
$ git push origin ticket_x --delete
```

### **8. Publicar master novamente**
