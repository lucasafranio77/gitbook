O Adobe XD é a nossa ferramenta de prototipação que utilizamos na nossa equipe.

Os protótipos finalizados estão upados na conta da infocorp, e ele funcionará como nosso centralizador dos protótipos e design specs dessa forma facilitando o acesso da equipe toda.

Para acessar é só seguir esses passos.

```
Acessar: https://assets.adobe.com/links

Login: 
Senha: 
```

Vá do conceito ao protótipo com o Adobe XD, a solução UX / UI completa para projetar websites, aplicativos móveis e muito mais. Agora o Adobe XD tem um plano Starter que é gratuito e permite um protótipo e design specs na cloud da adobe.

![xd-detailsvideo-1-878x494](/uploads/98019fd67671b5d7b61aebbf121654c9/xd-detailsvideo-1-878x494.gif)

### Links úteis

* **[Site da Adobe XD](https://www.adobe.com/br/products/xd.html)**
* **[Blog da Adobe XD](https://theblog.adobe.com/creative-cloud/xd/)**
* **[Curso de Adobe XD](https://www.youtube.com/watch?v=nl-Is79eyD8&list=PL9rc_FjKlX3-K25DZVcNlsVDItg9OlZiW)** 
* **[RIP Sketch? Migrando pro Adobe XD em 3… 2… 1…](https://medium.com/ui-lab-school/rip-sketch-migrando-pro-adobe-xd-em-3-2-1-505e011c3d4d)**
* **[Twitter do Andrew Shorten :](https://twitter.com/ashorten)** este aqui é o twitter de um dos líderes do Adobe XD e ele sempre publica as novas features que irão ser lançadas
* **[Twitter do Demian Borba :](https://twitter.com/demianborba)** este é um brasileiro que participa do projeto e sempre publica coisas sobre o Adobe XD
* [A Quick Start Guide to Adobe XD: 8 Essential Tutorials for Experience Design CC - Blog Storyblocks](https://blog.storyblocks.com/tutorials/a-quick-start-guide-to-adobe-xd-8-essential-tutorials-for-experience-design-cc/)
* [Adobe XD Tutorials - Dansky Youtube](https://www.youtube.com/playlist?list=PLkiM1tZke4mivrZRPcqp_8oHFxlD8-IP5)
* [Adobe XD Tutorials - Help Adobe](https://helpx.adobe.com/xd/tutorials.html)
* [Adobe XD Guide](https://www.xdguru.com/adobe-xd-guide/)
* [Adobe XD Tutorials - XD Guru](https://www.xdguru.com/adobe-xd-tutorials/)
* [UX Best Practices with Adobe XD - Blog Adobe](https://helpx.adobe.com/xd/how-to/ux-best-practices.html)
