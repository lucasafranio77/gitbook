Lista de bons materiais e links compartilhados por nossa equipe que dizem respeito sobre UX Design e outros assuntos relacionados.

![image](./images/ux-ui.png)

![image](./images/ux-ui1.png)

![image](./images/ux-ui2.png)

![image](./images/ux-ui3.png)

### **Menu**
- [Links Importantes](#links-importantes)
- [Cursos](#cursos)
- [Palestras e Apresentações em eventos](#palestras-e-apresenta%C3%A7%C3%B5es-em-eventos)
- [Lista de blogs](#lista-de-blogs)
- [Links úteis](#links-%C3%BAteis)

## Links Importantes

* [Roadmap UI/UX Designer](https://github.com/togiberlin/ui-ux-designer-roadmap) - é uma curadoria do que deve se aprender para trabalhar nesta área
* [Helping You Design Meaningful Experiences](http://handrailux.com/ux-guide.php?ref=hackingui) - Aproveite a pesquisa do usuário durante todo o processo de design para aumentar sua probabilidade de sucesso.
* [Design Centrado no Usuário - Um Guia de Referências](https://docs.google.com/presentation/d/1NSni67AeGUEqBsy3aiP0IjyxuSoF7-mgH40OOgZray4/edit?usp=sharing)
* [UI Designer Roadmap - Hackr.io](https://hackr.io/roadmaps/ui-designer-roadmap)
* [UX Designer Roadmap - Hackr.io](https://hackr.io/roadmaps/ux-designer-roadmap)

**[⬆ Voltar ao topo](#menu)**

## Cursos

* [Curso gratuito de UX UI Design - TIM TEC](http://timtec.com.br/curso/ux-e-ui-design/)
* [Material Design para Desenvolvedores Android by Google - Udacity](https://br.udacity.com/course/material-design-for-android-developers--ud862)
* [Rapid Prototyping by Google - Udacity](https://br.udacity.com/course/rapid-prototyping--ud723)
* [Fundamentos do Design Responsivo para a Web by Google - Udacity](https://br.udacity.com/course/responsive-web-design-fundamentals--ud893)

**[⬆ Voltar ao topo](#menu)**

## Palestras e Apresentações em eventos

* [UX para todos (na prática) - Beatriz Lonskis (UX Conf 2017)](https://www.infoq.com/br/presentations/ux-para-todos-na-pratica)

**[⬆ Voltar ao topo](#menu)**

## Lista de blogs

**[⬆ Voltar ao topo](#menu)**

## Links úteis

* [UX Design Guide - Austin Knight](https://austinknight.com/ux-design-guide/)
* [UX Checklist](http://uxchecklist.github.io/) - uma lista de UX para projetos
* [Web Field Manual](https://webfieldmanual.com/design)
* [Livros de UI, UX, Front-End e outros assuntos que você deveria conhecer - UI Lab School](https://medium.com/ui-lab-school/livros-de-ui-ux-front-end-e-outros-assuntos-que-voc%C3%AA-deveria-conhecer-9a69946fcd3f)
* [Por onde começar na área de UX? - UX Blog](https://uxdesign.blog.br/por-onde-come%C3%A7ar-na-%C3%A1rea-de-ux-49237cc7de90)
* [Lista Awesome Product Design - Github](https://github.com/teoga/awesome-product-design) - Uma coleção de marcadores, recursos, artigos para designers de produtos.
* [Guia de estudos UX Design PT-BR - Github](https://github.com/PopUpDesign/Bookmarks) - Guia de estudos e ferramentas sobre design, UX e Front-end
* [Questionário para Design System](https://github.com/bradfrost/design-system-questionnaire) - uma lista de perguntas para ajudar as empresas montarem seu sistema de design de forma eficaz
* [Curadoria de lista para UI/UX Designers - Github](https://github.com/gregjw/ui-ux)
* [Material para montar o seu evento de UX Design](https://github.com/humphreybc/uxdesignday-resources)
* [Linha para ensinar novos alunos sobre UX Design](https://github.com/RavensbourneWebMedia/UX-design)
* [Guia Completo de Como o UX Designer Trabalha – Problema, Pesquisa, Design e Testes - Designr](http://designr.com.br/guia-completo-de-como-o-ux-designer-trabalha-problema-pesquisa-design-e-testes/)
* [A Comprehensive Guide To User Experience Design - Smashing Magazine](https://www.smashingmagazine.com/2018/02/comprehensive-guide-user-experience-design/)
* [Getting The Most Out Of On-Site UX Research - Usability Geek](https://usabilitygeek.com/on-site-ux-research/)
* [User Interface Design Guidelines: 10 Rules of Thumb - interaction design foundation](https://www.interaction-design.org/literature/article/user-interface-design-guidelines-10-rules-of-thumb)
* [EBUNKING STEREOTYPES ABOUT THE IDENTITY OF UI AND UX DESIGN](https://www.cleveroad.com/blog/debunking-stereotypes-about-the-identity-of-ui-and-ux-design?ref=hackingui)
* [UI Goodies](http://uigoodies.com/content.html) - Diretório de recursos de UI
* [Adele](https://adele.uxpin.com) - O repositório de sistemas de design disponíveis publicamente e bibliotecas de padrões
* [The Design Gnome Project - Invision](https://www.invisionapp.com/enterprise/design-genome)

**[⬆ Voltar ao topo](#menu)**