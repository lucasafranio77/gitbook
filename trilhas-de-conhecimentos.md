Aqui você encontrará algumas dicas de como aprimorar o seu conhecimento, vai ter dicas de cursos, dicas de livros, podcasts, videos e muito mais.

### **Menu**
- [**Ferramentas de estudos gerais**](#ferramentas-de-estudos-gerais)
- [**Introdução**](#introdu%C3%A7%C3%A3o)
- [**Front-end**](#front-end)
  - [**HTML** - material de estudo sobre HTML](#html---material-de-estudo-sobre-html)
  - [**CSS** - material de estudo sobre CSS](#css---material-de-estudo-sobre-css)
  - [**Javascript** - material de estudo sobre Javascript](#javascript---material-de-estudo-sobre-javascript)
  - [**SVG**](#svg)
- [**Back-end**](#back-end)
  - [**Python**](#python)
    - [**Framework Síncrono**](#framework-s%C3%ADncrono)
  - [**Ruby**](#ruby)
    - [**Frameworks**](#frameworks)
  - [**Node.js** - JavaScript runtime built on Chrome's V8 JavaScript engine.](#nodejs---javascript-runtime-built-on-chromes-v8-javascript-engine)
    - [**Gerenciador de Pacotes**](#gerenciador-de-pacotes)
    - [**Testes**](#testes)
  - [**Servidores Web**](#servidores-web)
  - [**GraphQL**](#graphql)
  - [**Docker**](#docker)
  - [**Banco de Dados Relacionais**](#banco-de-dados-relacionais)
  - [**Banco de Dados NoSQL**](#banco-de-dados-nosql)
- [**DevOps**](#devops)
  - [**Sistemas Operacionais**](#sistemas-operacionais)
  - [**Nuvem**](#nuvem)
  - [**Automação**](#automa%C3%A7%C3%A3o)
  - [**Continuous Integration and Continuous Delivery**](#continuous-integration-and-continuous-delivery)
  - [**Monitoramento de Alertas**](#monitoramento-de-alertas)
  - [**Containers**](#containers)
  - [**Gerenciamento de Cluster**](#gerenciamento-de-cluster)
  - [**Servidores Web**](#servidores-web-1)
  - [**SSH**](#ssh)
  - [**Amor pelo Terminal**](#amor-pelo-terminal)
- [**React Developer Roadmap**](#react-developer-roadmap)
- [**Vue Developer Roadmap**](#vue-developer-roadmap)
- [**Mobile**](#mobile)
  - [**Android**](#android)
  - [iOS](#ios)
- [Ciência de Dados](#ci%C3%AAncia-de-dados)
- [UX/UI Design](#uxui-design)
- [Business](#business)
- [Certificações](#certifica%C3%A7%C3%B5es)

## **Ferramentas de estudos gerais**

- [Pretty Awesome Lists](https://www.prettyawesomelists.com/)
- [Quick Code](http://www.quickcode.co/) - lista de cursos gratuitos
- [Courseroot](https://courseroot.com/) - O maior banco de dados de cursos on-line para você filtrar com base no preço, dificuldade, qualidade do certificado e muito mais
- [Hackr.io](https://hackr.io/)
- [Dev.Tube](https://dev.tube)
- [Product Frameworks](https://www.product-frameworks.com/) - Uma coleção de recursos e práticas recomendadas de empresas e especialistas líderes para ajudá-lo a criar produtos.
- [Student Developer Pack - Github Education](https://education.github.com/pack) - oferecer aos alunos acesso gratuito às melhores ferramentas de desenvolvedor em um só lugar, para que possam aprender fazendo.
- [Joguinho do sapinho para aprender flex-box](https://flexboxfroggy.com/)
- [Linkedin Learning](https://www.linkedin.com/learning)
- [Studdent](https://studddent.com/) - site que reune descontos para estudantes em plataformas
- [freecodecamp.org](https://www.freecodecamp.org)
  - Responsive Web Design Certification (300 hours)
  - Javascript Algorithms And Data Structures Certification (300 hours)
  - Front End Libraries Certification (300 hours)
  - Data Visualization Certification (300 hours)
  - Apis And Microservices Certification (300 hours)
  - Information Security And Quality Assurance Certification (300 hours)
  - Coding Interview Prep (Thousands of hours of challenges)


**[⬆ Voltar ao topo](#menu)**

## **Introdução**

![mapa-introducao](./images/mapa-introducao.png)

Esses são os pontos básicos que todos tem que saberem. Abaixo você encontra alguns materiais de cada ponto levantado na imagem:

- [Git 101](Git101)
- [Curso de Linha de Comando Linux Básico](https://br.udacity.com/course/linux-command-line-basics--ud595)
- [Workshop sobre shell](https://br.udacity.com/course/shell-workshop--ud206)
- [Roadmap of Web Applications on Mobile - W3C](https://www.w3.org/Mobile/roadmap/)
- [The 2018 Web Developer Roadmap](https://codeburst.io/the-2018-web-developer-roadmap-826b1b806e8d)
- [Google Shell Style Guide](https://google.github.io/styleguide/shell.xml)

**[⬆ Voltar ao topo](#menu)**

## **Front-end**

![mapa-frontend](./images/mapa-frontend.png)

- [Front End Developer Roadmap - Hackr.io](https://hackr.io/roadmaps/front-end-developer-roadmap)

### **[HTML](Html)** - material de estudo sobre HTML
### **[CSS](Css)** - material de estudo sobre CSS
- [Flexbox](https://github.com/afonsopacifer/awesome-flexbox#readme)
- **Pré-processadores**
    - [Sass](https://github.com/Famolus/awesome-sass#readme)
    - [Less](https://github.com/LucasBassetti/awesome-less#readme)
    - [PostCSS](https://github.com/jdrgomes/awesome-postcss#readme)
- **Metodologias**
    - [BEM](- [BEM](https://github.com/sturobson/BEM-resources#readme))
### **[Javascript](Javascript)** - material de estudo sobre Javascript
- [ES6](https://github.com/addyosmani/es6-tools#readme)
- **Carregador de módulos**
    - [Webpack](https://github.com/webpack-contrib/awesome-webpack#readme) - O webpack é um empacotador de módulos. O webpack usa módulos com dependências e gera ativos estáticos representando esses módulos.
    - [Browserify](https://github.com/browserify/awesome-browserify#readme) - Module bundler.
- **Gerenciador de pacotes**
    - [NPM](https://github.com/sindresorhus/awesome-npm#readme)
- **Execultador de tarefas**
    - [Gulp](http://alferov.github.io/awesome-gulp/)
    - [npm scripts](https://github.com/RyanZim/awesome-npm-scripts#readme) - Task runner.
- **Framework**
    - [Angular](https://github.com/gdi2290/awesome-angular#readme)
    - [React Native](https://github.com/jondot/awesome-react-native#readme)
    - [React](https://github.com/enaqx/awesome-react#readme) - App framework.
        - [Relay](https://github.com/expede/awesome-relay#readme) - Framework for building data-driven React apps.
        - [Redux](https://github.com/brillout/awesome-redux#readme) - State container for JavaScript apps.
    - [Vue.js](https://github.com/vuejs/awesome-vue#readme)

### **[SVG](https://github.com/willianjusten/awesome-svg#readme)**

- [D3](https://github.com/wbkd/awesome-d3#readme) - Library for producing dynamic, interactive data visualizations.


**[⬆ Voltar ao topo](#menu)**

## **Back-end**

![mapa-backend](./images/mapa-backend.png)

### **[Python](https://github.com/vinta/awesome-python#readme)**

- [Google's Python Class](https://developers.google.com/edu/python/)

#### **Framework Síncrono**

- [Flask](https://github.com/humiaozuzu/awesome-flask#readme)
- [Django](https://github.com/rosarior/awesome-django#readme)
- [Pyramid](https://github.com/uralbash/awesome-pyramid#readme)
  
### **[Ruby](https://github.com/markets/awesome-ruby#readme)**

#### **Frameworks**

- [Ruby on Rails](https://github.com/ekremkaraca/awesome-rails#readme) - Web app framework for Ruby.

### **[Node.js](https://github.com/sindresorhus/awesome-nodejs#readme)** - JavaScript runtime built on Chrome's V8 JavaScript engine.

#### **Gerenciador de Pacotes**

- [npm](https://github.com/sindresorhus/awesome-npm#readme) - Package manager.

#### **Testes**

- [AVA](https://github.com/avajs/awesome-ava#readme) - Test runner.

### **Servidores Web**

- [nginx](https://github.com/fcambus/nginx-resources#readme)

### **[GraphQL](https://github.com/chentsulin/awesome-graphql#readme)**

- [How to GraphQL](https://www.howtographql.com/)

### **[Docker](https://github.com/veggiemonk/awesome-docker#readme)**

### **Banco de Dados Relacionais**

- [PostgreSQL](https://github.com/dhamaniasad/awesome-postgres#readme) - Object-relational database.
- [MySQL](https://github.com/shlomi-noach/awesome-mysql/blob/gh-pages/index.md)

### **Banco de Dados NoSQL**

- [MongoDB](https://github.com/ramnes/awesome-mongodb#readme)
- [RethinkDB](https://github.com/d3viant0ne/awesome-rethinkdb#readme)
- [CouchDB](https://github.com/quangv/awesome-couchdb#readme)

**[⬆ Voltar ao topo](#menu)**

## **DevOps**

![mapa-devops](./images/mapa-devops.png)

### **Sistemas Operacionais**

- [Linux](https://github.com/aleksandar-todorovic/awesome-linux#readme)
	- [Containers](https://github.com/Friz-zy/awesome-linux-containers#readme)

### **Nuvem**

- [Amazon Web Services](https://github.com/donnemartin/awesome-aws#readme)
- [Heroku](https://github.com/ianstormtaylor/awesome-heroku#readme) - Cloud platform as a service.
- [DigitalOcean](https://github.com/jonleibowitz/awesome-digitalocean#readme) - Cloud computing platform designed for developers.

### **Automação**

- [PowerShell](https://github.com/janikvonrotz/awesome-powershell#readme) - Cross-platform object-oriented shell.
- [SaltStack](https://github.com/hbokh/awesome-saltstack#readme) - Python-based config management system.
- [Terraform](https://github.com/shuaibiyy/awesome-terraform#readme) - Tool for building, changing, and versioning infrastructure.

### **[Continuous Integration and Continuous Delivery](https://github.com/ciandcd/awesome-ciandcd#readme)**

### **Monitoramento de Alertas**

- [Prometheus](https://github.com/roaldnefs/awesome-prometheus#readme) - Open-source monitoring system.

### **[Containers](https://github.com/Friz-zy/awesome-linux-containers#readme)**

- [Docker](https://github.com/veggiemonk/awesome-docker#readme)
  
### **Gerenciamento de Cluster**

- [Kubernetes](https://github.com/ramitsurana/awesome-kubernetes#readme)

### **Servidores Web**

- [nginx](https://github.com/fcambus/nginx-resources#readme)

### **[SSH](https://github.com/moul/awesome-ssh#readme)**

### **Amor pelo Terminal**

- [Vim](https://github.com/mhinz/vim-galore#readme)

**[⬆ Voltar ao topo](#menu)**

## **React Developer Roadmap**

![React Developer Roadmap](https://github.com/adam-golab/react-developer-roadmap/raw/master/roadmap.png)

## **Vue Developer Roadmap**

![Vue Developer Roadmap](./images/roadmap.svg)


## **Mobile**

![image](./images/mobile-developer.png)

**[⬆ Voltar ao topo](#menu)**

### **Android**

- [HOW TO BECOME AN ANDROID DEVELOPER: A COMPLETE ROADMAP 2018](http://www.mrshuvo.com/android-development/become-a-android-developer/)
- [Learning Android Development in 2018 — Part 1](https://android.jlelse.eu/learning-android-development-in-2018-part-1-83a514f6a205)
- [[Roadmap] Learn Android Basics to Android Developer With Udacity](https://medium.com/@29nikhilsingh/roadmap-learn-android-basics-to-android-developer-with-udacity-fe64fbee491f)
- [Android App Developer Roadmap - Hackr.io](https://hackr.io/roadmaps/android-app-developer-roadmap)

### iOS

![image](./images/ios-developer.png)

![image](./images/ios-dev-niveis.png)

**[⬆ Voltar ao topo](#menu)**

## [Ciência de Dados](Ciência de dados)

## [UX/UI Design](Ux ui Design)

![image](./images/ux-design.png)

Segundo a Wikipedia, User Experience envolve os sentimentos de uma pessoa em relação à utilização de um determinado produto, sistema ou serviço. A experiência do usuário destaca os aspectos afetivos, experiências, significativos e valiosos de interação humano-computador e propriedade do produto.

**[⬆ Voltar ao topo](#menu)**

## Business

## Certificações

- [Certificações gratuitas de gerenciamento de projetos que você não pode perder](http://cio.com.br/gestao/2017/04/24/certificacoes-gratuitas-de-gerenciamento-de-projetos-que-voce-nao-pode-perder/)
- [Veja aqui como conseguir suas Certificações em TI de graça](http://suporteninja.com/veja-aqui-como-conseguir-sua-certificacoes-em-ti-de-graca/)
- [Veja como conseguir sua Certificação em TI de graça (Parte 2)](http://suporteninja.com/veja-como-conseguir-sua-certificacao-em-ti-de-graca-parte-2/)
- [Certificação de Graça? Sim, é possível. Veja Como...](https://pt.linkedin.com/pulse/certifica%C3%A7%C3%A3o-de-gra%C3%A7a-sim-%C3%A9-poss%C3%ADvel-veja-como-carlos-cedro)

**[⬆ Voltar ao topo](#menu)**