Este aqui é o **hub MPX Brasil**!

Um local para centralizarmos as informações sobre a **NOSSA** empresa. 
Isso aqui é pra ser tipo a Wikipédia sobre a MPX Brasil.

E o melhor, é **VOCÊS** que nos ajudam a responder essas e muitas outras perguntas.
Então não deixe de ver essas informações todos os dias neste mesmo batcanal.

**Menu**
- [**Dicas de GIT**](#dicas-de-git)
- [**Workflow GIT**](#workflow-git)
- [**Trilha de Conhecimento**](#trilha-de-conhecimento)
- [**Dicas para sua carreira e linkedin**](#dicas-para-sua-carreira-e-linkedin)
- [**Boas extensões para o Chrome**](#boas-extens%C3%B5es-para-o-chrome)
- [**Adobe XD**](#adobe-xd)
- [**Modelo de README para projetos**](#modelo-de-readme-para-projetos)

### **Dicas de GIT**

- **[Git 101](git101.md)** - Links e Materiais sobre git by @lucasafranio77
- **[Iniciar Repositório](Iniciar Repositório.md)** by @GabrielaGSantos
- **[Editar Repositório](Editar Repositório.md)** by @GabrielaGSantos
- **[Subir Repositório](Subir Repositório.md)** by @GabrielaGSantos

### **Workflow GIT**
- [**Informações Importantes**](Workflow-GIT---Informações-Importantes) by Renan

### **[Trilha de Conhecimento](trilhas-de-conhecimentos.md)**

Vários materiais para estudo de várias áreas da TI.
by @lucasafranio77

### **[Dicas para sua carreira e linkedin](Dicas-para-sua-carreira.md)**

### **[Boas extensões para o Chrome](boas-extensoes.md)**

### **[Adobe XD](adobe-XD.md)**

### **[Modelo de README para projetos](modelo-README.md)**

Esse modelo foi criado para facilitar o acesso de informações pelos participantes no projeto, caso tenha um projeto novo, copie esse modelo e use