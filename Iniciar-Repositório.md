
# Tutoriais para deploy MPX Brasil
> Guia de como começar seu repositório para o desenvolvimento do site local.
__________________
# Iniciar Projeto

### 1. Crie um novo projeto
Para que o projeto seja desenvolvido com Git `crie um novo projeto no gitlab` com seu nome específico, seguindo o padrão de nomenclatura da MPX Brasil 
> Exemplo: pm_jangada_mt_site;

É importante lembrar de criar o repositório `PRIVADO`.
_____
### 2. Arquivos iniciais
Junto com os arquivos iniciais, como admin, pasta css, index.php, etc. Coloque o arquivo __`.gitignore`__ na pasta raiz do seu projeto com o seguinte conteúdo:

```sh
error_log
cache
fotos_csv/
fotos_*
*.csv
*.txt
```
##### É de extrema importância não ignorar este passo. Ignorar este passo resultará em um repositório muito grande na hora da publicação. 
> Obs: este erro já foi cometido, resultando em um repositório de mais de 10GB
_____
 
### 3. Desenvolvimento
Durante o desenvolvimento do projeto lembre-se de realizar commits regularmente.

```sh
$ git add .
$ git commit -m "Realizada tarefa ABC"
$ git push origin master
``` 
_____
### 4. Publicar
Para publicar o site retire do arquivo __`.gitignore`__ das fotos e documentos, deixe apenas:
```sh
error_log
cache
```

Após modificar o  __.gitignore__ siga o tutorial [Subir Repositório](Subir Repositório)
______
### 5. Suporte

Para realizar suporte siga o tutorial [Editar Repositório](Editar Repositório)
____________

##### Mais informações sobre Git: (http://rogerdudler.github.io/git-guide/index.pt_BR.html)

