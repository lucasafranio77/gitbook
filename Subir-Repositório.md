# Tutoriais para deploy MPX Brasil
> Guia de como subir seu repositório para o servidor de produção.

> Recomenda-se que os passos a seguir sejam realizados após a conclusão do desenvolvimento local.

### Pré-requisitos

Realizar o commit em master

```sh
$ git add .
$ git commit -m "Desenvolvimento local concluído"
$ git push origin master
```

Também é interessante criar uma tag para identificar este commit. Para isso primeiramente copie a chave do seu último commit

```sh
$ git log --pretty="%h - %s" -1
1a0fb5a - Desenvolvimento local concluído

$ git tag 1.0.0 1a0fb5a
```

## Subir repositório
_______________
### 1. Crie um novo branch chamado homologacao
> Para que haja facilidade em realizar suportes posteriores: não exclua esta branch. Esta branch servirá apenas como auxiliar de suporte.

```sh
$ git checkout -b homologacao
```
________
### 2. Envie o branch
> Um branch não está disponível a outros a menos que você envie o branch para seu repositório remoto.

```sh
$ git push origin homologacao
```
_________
### 3. Confirme o branch
> Confira em seu repositório remoto se o branch homologacao foi realmente criado.
__________
### 4. Retornar para master 
```sh
$ git checkout master
```
___________
### 5. Modificar BD e URL
> Abra novamente seu editor de texto e realize as mudanças para que o site possa ser publicado: como senha e nome banco de dados, url do site, etc.
__________
### 6. Commit para master
> Agora seu master será a sua produção, ou seja, no ramo master haverá todas as configurações para que o site funcione no ar. A partir de agora NÃO modifique arquivos do master manualmente. 

```sh
$ git add .
$ git commit -m "Repositório master modificado para produção"
$ git push origin master
```
> Se quiser pode criar uma tag disto também.

> Pode realizar os passos de publicação da master.
______________
### 7. Suporte
Para realizar suporte siga o tutorial [Editar Repositório](Editar Repositório)